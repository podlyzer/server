import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import PostMessage from '../models/postMessage.js';
import upload from './upload.js';
import transcript from './transcript.js';
import transemail from './transemail.js';
import sgMail from "@sendgrid/mail";

dotenv.config();

sgMail.setApiKey(process.env.SENDGRID_API_KEY);
const router = express.Router();

export const getPosts = async (req, res) => {

    const { page } = req.query;
    try {
        const LIMIT = 12;
        const startIndex = (Number(page) - 1) * LIMIT;  // get the starting index of every page

        const total = await PostMessage.countDocuments({});
        const posts = await PostMessage.find(/* { category: req.category } */)
                                        .sort({episodeTitle: 1})
                                        .limit(LIMIT)
                                        .skip(startIndex);

        res.json({ data: posts, currentPage: Number(page), numberOfPages: Math.ceil(total / LIMIT)});
    } catch (error) {    
        res.status(404).json({ message: error.message });
    }
}

/* export const getPostsBySortFilter = async (req, res) => {

    const {sort, sortDirection, category} = req.query;

    if (!sort) {
        sort = "episodeTitle";
    }

    if (!sortDirection) {
        sortDirection = 1;
    }   

    try {

        const posts = await PostMessage.find({ category: req.category } )
                                        .collation({locale: "en"})
                                        .sort({sort, sortDirection});

        res.json({ data: posts });
    } catch (error) {    
        res.status(404).json({ message: error.message });
    }
}
 */
export const getPostsBySearch = async (req, res) => {
    //const { searchQuery, keywords } = req.query;
    //for getting string type result
    const searchQuery = req.query.searchQuery;

    try {
        //const episodeTitle = new RegExp(searchQuery, 'i');
        //const podcastTitle = new RegExp(searchQuery, 'i');
        //const category = new RegExp(searchQuery, 'i');
        //const description = new RegExp(searchQuery, 'i');
        //const keywords = new RegExp(searchQuery, 'i');
        //const transcript = new RegExp(searchQuery, 'i');

        //const posts = await PostMessage.find({  $or: [{ episodeTitle }, { podcastTitle }, { category }, { keywords }, { description }, { transcript }] });
        
        // for searching with text indexes and rank based on weighted scoring
        const posts = await PostMessage.find(
            {$text: {$search:searchQuery, $caseSensitive: false}},
            {score: {$meta: "textScore"}})
            .sort({ score: { $meta: "textScore"}});
        res.json({ data: posts });
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getCreatorsPostsBySearch = async (req, res) => {
    //for getting string type result
    const searchQuery = req.query.searchQuery;

    try {
       
        // for searching with text indexes and rank based on weighted scoring
        const posts = await PostMessage.find({  $and: [
            {creator: req.userId },
            {$text: {$search:searchQuery, $caseSensitive: false}}]},
            {score: {$meta: "textScore"}})
            .sort({ score: { $meta: "textScore"}});
        res.json({ data: posts });
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}


export const getCreatorPosts = async (req, res) => {
    const { page } = req.query;

    try {

        const LIMIT = 12;
        const startIndex = (Number(page) - 1) * LIMIT;  // get the starting index of every page
        const total = await PostMessage.countDocuments({ creator: req.userId });
        const posts = await PostMessage.find({ creator: req.userId }).sort({ createdAt: -1 }).limit(LIMIT).skip(startIndex);

        res.json({ data: posts, currentpage: Number(page), numberOfpages: Math.ceil(total / LIMIT) });
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}

export const getCreatorPost = async (req, res) => {
    const { id } = req.params;

    try {
        const post = await PostMessage.findById(id);

        res.status(200).json(post);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}




export const getPost = async (req, res) => {
    const { id } = req.params;

    try {
        const post = await PostMessage.findById(id);

        res.status(200).json(post);
    } catch (error) {
        res.status(404).json({ message: error.message });
    }
}



export const createPost = async (req, res) => {
    const post /*{ creator, podcastTitle, episodeTitle, description, link, keywords, selectedFile, podFile}*/ = await req.body;
    var newtranscript = '';
    var podlink = '';

    if (!post.podFile == '') {
        //get URL from uploading file
        const uploadURL = await upload(post.episodeTitle, post.podFile);
        //get transcription
        newtranscript = await transcript(uploadURL);
        podlink = `https://storage.googleapis.com/${uploadURL}`;
        const msg = await transemail(newtranscript, post.episodeTitle, req.useremail, req.username);
        sgMail.send(msg);
    } else {
        newtranscript = '';
        podlink = '';
    }

    const newPostMessage = new PostMessage({
        podcastTitle: post.podcastTitle, episodeTitle: post.episodeTitle,
        description: post.description, link: post.link,
        keywords: post.keywords, selectedFile: post.selectedFile,
        category: post.category, podlink: podlink, podcastID: post.podcastTitle.replace(/\s/g, ''),
        transcript: newtranscript, creator: req.userId, createdAt: new Date().toISOString()
    });

    try {
        await newPostMessage.save();
        res.status(201).json(newPostMessage);
    } catch (error) {
        res.status(409).json({ message: error.message });
    }
}

export const likePost = async (req, res) => {
    const { id } = req.params;

    if (!req.userId) {
        return res.json({ message: "Unauthenticated" });
    }

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);

    const post = await PostMessage.findById(id);

    const index = post.likeCount.findIndex((id) => id === String(req.userId));

    if (index === -1) {
        post.likeCount.push(req.userId);
    } else {
        post.likeCount = post.likeCount.filter((id) => id !== String(req.userId));
    }
    const updatedPost = await PostMessage.findByIdAndUpdate(id, post, { new: true });
    res.status(200).json(updatedPost);
}
export default router;