import speech from '@google-cloud/speech';
import dotenv from 'dotenv';

dotenv.config();
// Import the Google Cloud client library
const googlespeech = speech.v1p1beta1;

async function transcript (uploadURL) {
    if (uploadURL == ''){
        return '';
    } else {
        // Creates a speech client
        const client = new googlespeech.SpeechClient({projectId: process.env.GOOGLE_CLOUD_PROJECT, credentials: {client_email: process.env.GCLOUD_CLIENT_EMAIL, private_key: process.env.GCLOUD_PRIVATE_KEY}});

        const gcsUri = `gs://${uploadURL}`;
        const encoding = 'LINEAR 48';
        const sampleRateHertz = 48000;
        const languageCode = 'en-US';

        const audio = {
            uri: gcsUri,
        };

        const config = {
            encoding: encoding,
            sampleRateHertz: sampleRateHertz,
            languageCode: languageCode,
            enableAutomaticPunctuation: true,
            enableWordConfidence: true,
            };

        const request = {
        config: config,
        audio: audio,
        };

        // Detects speech in the audio file. This creates a recognition job that you
        // can wait for now, or get its result later.
        const [operation] = await client.longRunningRecognize(request);
        // Get a Promise representation of the final result of the job
        const [response] = await operation.promise();
        const transcription = response.results
                .map(result => result.alternatives[0].transcript)
                .join('\n');
        return transcription;
    };
}
export default transcript;