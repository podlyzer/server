async function transemail (newtranscript, episodeTitle, useremail, username){

const transcriptarray = newtranscript.replace(/([.?!])\s*(?=[A-Z])/g, "$1 |").split("|")
const text = transcriptarray.join('\n');
const transcriptstring = Buffer.from(text, "utf8");
const attachment = transcriptstring.toString("base64");

const transmsg = {
    to: {email: useremail},
    from: { name: 'Podlyzer', email: 'corz.nina@gmail.com', },
    subject: 'Transcription is READY!',
    html: `<p>Hi ${username},</p>
           <p></p>
           <p>Your <strong>${episodeTitle}</strong> transcript is ready! </p>
           <p></p>
           <p>Hope you enjoy our transcription service!</p>
           <p></p>
           <p>Love,</p>
           <p>Podlyzer Team</p>`,
    attachments: [
        {
          content: attachment,
          filename: `${episodeTitle}.txt`,
          type: 'plain/text',
          disposition: "attachment"
        }
      ]
  }; 

return transmsg;
}

export default transemail;

