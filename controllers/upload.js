import dotenv from 'dotenv';
import {Storage} from'@google-cloud/storage';
import mimeTypes from 'mimetypes';
import fs from 'fs';

dotenv.config();

// Creates a storage client
const bucketName = process.env.GCLOUD_STORAGE_BUCKET;
const storage = new Storage({projectId: process.env.GOOGLE_CLOUD_PROJECT, credentials: {client_email: process.env.GCLOUD_CLIENT_EMAIL, private_key: process.env.GCLOUD_PRIVATE_KEY}});
const myBucket = storage.bucket(bucketName);

async function filecreate(filename, audioBuffer){
   fs.writeFileSync(filename, audioBuffer);

  try {
    fs.accessSync(filename, fs.constants.R_OK | fs.constants.W_OK);
    return filename;
  } catch (err) {
    console.log("no access:", filename);
    console.error(err);
  }
};

async function gcsupload(nfilename){
  await myBucket.upload(nfilename, {
    destination: nfilename,
  });
  return `${bucketName}/${nfilename}`;
};

async function deletelocal(nfilename){
fs.unlink(nfilename,function(err){
  console.log(`${nfilename} deleted locally`);
});
};

async function upload (episodeTitle, podFile) {
  const date = new Date();
  const createddate = +date.getFullYear()+("0" + (date.getMonth() + 1)).slice(-2)+("0" + date.getDate()).slice(-2);

  // for filename definition
  const Stringcode = podFile.split(';base64,').pop();
  const mimeType = podFile.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/)[1];
  const filename = episodeTitle.replace(/\s/g,'_')+'_'+createddate+'.'+mimeTypes.detectExtension(mimeType);
  
  // for audio content into binary code
  const audioBuffer = new Buffer.from(Stringcode, 'base64');

  const nfilename = await filecreate(filename, audioBuffer);
  const uploadURL = await gcsupload(nfilename);
  
  deletelocal(nfilename);
  
  return uploadURL;  

};
export default upload;