import bcrypt from "bcryptjs"; //encrypting password
import jwt from "jsonwebtoken"; //json web token to store user in browser for a certain period of time 
import dotenv from 'dotenv';
import User from "../models/user.js";
import sgMail from "@sendgrid/mail";

dotenv.config();
const secret = 'test';

//const API_KEY = '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'
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

export const signin = async (req, res) => {
  const { email, password } = req.body;

  try {
    const existingUser = await User.findOne({ email });  //existing User was previously oldUser, just in case of conflicts

    if (!existingUser) return res.status(404).json({ message: "User doesn't exist" });

    if (!existingUser.activated)  return res.status(404).json({ message: "Account not activated. Please Check your emails." });

    const isPasswordCorrect = await bcrypt.compare(password, existingUser.password); //check if always false

    if (!isPasswordCorrect) return res.status(400).json({ message: "Invalid credentials" });

    const token = jwt.sign({ email: existingUser.email, id: existingUser._id }, 'test', { expiresIn: "1h" }); //string test is the token secret 

    res.status(200).json({ result: existingUser, token });
  } catch (err) {
    res.status(500).json({ message: "Something went wrong." });
  }
};

export const signup = async (req, res) => {
  const { email, password, firstName, lastName } = req.body;

  try {
    if (!validEmail(email)) return res.status(401).send({msg:'Your Email is not valid'});

    const existingUser = await User.findOne({ email });
  
    if (existingUser) return res.status(400).json({ message: "User already exists." });

   // if (password != confirmPassword) return res.status(400).json({message: "Passwords don´t match."}); //check if != is correct

    const hashedPassword = await bcrypt.hash(password, 12)

    const result = await User.create({ email, password: hashedPassword, name: `${firstName} ${lastName}` });

    const activation_token = jwt.sign( { email: result.email, id: result._id }, secret , { expiresIn: "1h" } ); 

    const vermail = `${process.env.CLIENT_URL}/user/activate/${activation_token}`;

    const msg = {
      to: {email},
      from: { name: 'Podlyzer', email: 'corz.nina@gmail.com', },
      templateId: 'd-17393909020b46ad8c7f554787f69cb5', 
      dynamic_template_data: { url: vermail },
    }; 
     sgMail.send(msg);

    res.status(200).json({ result });
  } catch (error) {
    res.status(500).json({ message: "Something went wrong!!" });
    
    console.log(error);
  }
};

export const activateEmail = async (req, res) => {
  try {
      const {activation_token} = req.params
      const user = jwt.verify(activation_token, secret ) //process.env.ACTIVATION_TOKEN_SECRET
      const check = await User.findOne({activation_token})
      if(check) return res.status(400).json({msg:"This email already exists."})

      await User.findByIdAndUpdate(user.id, {
        activated: true
    })
      res.json({msg: "Account has been activated!"})

  } catch (err) {
      return res.status(500).json({msg: err.message})
  }
};


function validEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

