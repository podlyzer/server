import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import postRoutes from './routes/posts.js';
import userRoutes from './routes/users.js';
import creationhistoryRoutes from './routes/creationhistory.js';


const app = express();
dotenv.config();

app.use(bodyParser.json({ limit: "100mb", extended: true }));  
app.use(bodyParser.urlencoded({ limit: "100mb", extended: true }));
app.use(cors());

//Middleware
app.use(express.json());


// Routes
app.use('/posts', postRoutes);
app.use('/user', userRoutes);
app.use('/creationhistory', creationhistoryRoutes);


// const CONNECTION_URL = 'mongodb+srv://Podlyzer:podlyzer123@cluster0.fdfrl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'; 
const PORT = process.env.PORT || 5000;

mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message));
    
mongoose.set('useFindAndModify', false);

// https://mongodb.com/cloud/atlas

