import jwt from "jsonwebtoken";

const secret = 'test';

const auth = async (req, res, next) => {  //checking and then doing soemthing next
  try {
    const token = req.headers.authorization.split(" ")[1];   //checking if token is valid
    const isCustomAuth = token.length < 500; //our token length
    const activation_token = req.params;

    let decodedData;

    if (token && isCustomAuth && activation_token) {               //custom token
      decodedData = jwt.verify(token, secret);
      req.useremail = decodedData?.email;
      req.username = decodedData?.name;
      req.userId = decodedData?.id;
    } else {
      decodedData = jwt.decode(token); //google token
      req.useremail = decodedData?.email;
      req.username = decodedData?.name;
      req.userId = decodedData?.sub;
    }    

    next();
  } catch (error) {
    console.log(error);
  }
};

export default auth;