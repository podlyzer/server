import mongoose from 'mongoose';

const postSchema = mongoose.Schema({
    podcastID: {
        type:String,
        required:true
    },
    podcastTitle: {
        type:String,
        required:true
    },
    episodeTitle: {
        type:String,
        required:true
    },
    link: String,
    podlink: String,
    createdAt: {
        type: Date,
        default: new Date(),
    },
    description: {
        type:String,
        required:true
    },
    transcript: {
        type: String
    },
    selectedFile: {
        type:String
    },
    podFile: String,
    likeCount: {
        type: [String],
        default:[]
    },
    keywords:{
        type:[String]
    },
    creator: String,
    category: {
        type:String,
        required:true
    },
    
});

//for creating index in mongodb with weighted requirements
postSchema.index(
    {
      episodeTitle: "text",
      podcastTitle: "text",
      category: "text",
      keywords: "text",
      description: "text",
      transcript: "text"
    },
    { name: "TextIndex",
      weights: {
        episodeTitle: 7,
        podcastTitle: 6,
        category: 5,
        keywords: 4,
        description: 3,
        transcript: 2     
    }}
  , function(err, result){
    if (err) { return stepComplete(err); }
 
    stepComplete();
   });

var postMessage = mongoose.model('postmessages', postSchema);

export default postMessage;