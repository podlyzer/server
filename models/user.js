import mongoose from "mongoose";

const userSchema = mongoose.Schema({
  name: { type: String, required:  true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  id: { type: String },
  activated: { type: Boolean, default: false, required: true},
});

export default mongoose.model("User", userSchema);