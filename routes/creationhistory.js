import express from 'express';

import {getCreatorPosts, getCreatorPost, getCreatorsPostsBySearch} from '../controllers/posts.js';

const router = express.Router();
import auth from '../middleware/auth.js';

// http://localhost:5000/creationhistory

router.get('/search', auth, getCreatorsPostsBySearch);
router.get('/', auth, getCreatorPosts);
router.get('/:id', auth, getCreatorPost);

export default router;