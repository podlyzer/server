import express from 'express';
import {getPostsBySearch, getPosts, getPost, createPost,  likePost} from '../controllers/posts.js';
import auth from '../middleware/auth.js';
const router = express.Router();



// http://localhost:5000/posts
router.get('/search', getPostsBySearch);
/* router.get('/posts/', getPostsBySortFilter); */
router.get('/', getPosts);
router.get('/:id', getPost);

router.post('/', auth , createPost); 

router.patch('/:id/likePost', auth , likePost);


export default router;