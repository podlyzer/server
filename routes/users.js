import express from "express";
const router = express.Router();

import { signin, signup, activateEmail } from "../controllers/user.js";

router.post("/signin", signin);  //sending information to backend
router.post("/signup", signup);
router.get("/activate/:activation_token", activateEmail) //:activation_token

export default router;

// http://localhost:5000/user/activate/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJ3ZDg4NjI5QGVvb3B5LmNvbSIsImlkIjoiNjBkMDY5MmNmMzM2YjI1OTViZmNkMWY4IiwiaWF0IjoxNjI0MjcxMTQ4LCJleHAiOjE2MjQyNzQ3NDh9.KrpfEXBbSIAhSW32GLzDxqQUFOelrs2cLaMn8nU-oBo